import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-toggle-content',
  templateUrl: './toggle-content.component.html',
  styleUrls: ['./toggle-content.component.scss'],
})
export class ToggleContentComponent implements OnInit {


  @Input() show : Boolean;

  visible: boolean = false;

 
  constructor(private route: ActivatedRoute,
    private requestService: RequestServiceService) { }

  ngOnInit() {
   
  }


  toggle() {
    this.visible = !this.visible;
  }

}
