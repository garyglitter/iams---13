import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'toggle-content-history',
  templateUrl: './toggle-content-history.component.html',
  styleUrls: ['./toggle-content-history.component.scss'],
})
export class ToggleContentHistoryComponent implements OnInit {


  count = [1, 2, 3];

  expandContent: Boolean = true;

  visible: boolean = false;

  dummyData: Observable<PendingRequest[]>;

  fakeData :any;

  constructor(
    private http: HttpClient,
    private requestService: RequestServiceService
  ) {}

  ngOnInit() {
    this.dummyData = this.requestService.dummyData;
    this.requestService.loadAllRequest();

    this.requestService.fakeData().subscribe(data=>{
      this.fakeData = data
    })
  }

  toggle() {
    this.visible = !this.visible;
  }
}
