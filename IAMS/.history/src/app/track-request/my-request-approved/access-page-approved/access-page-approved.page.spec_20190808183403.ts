import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessPageApprovedPage } from './access-page-approved.page';

describe('AccessPageApprovedPage', () => {
  let component: AccessPageApprovedPage;
  let fixture: ComponentFixture<AccessPageApprovedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessPageApprovedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessPageApprovedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
