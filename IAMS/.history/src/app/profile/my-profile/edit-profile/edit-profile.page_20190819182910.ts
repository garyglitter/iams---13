import { Component, OnInit } from "@angular/core";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";
import { Storage } from "@ionic/storage";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { ImagePicker } from '@ionic-native/image-picker/ngx';
@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.page.html",
  styleUrls: ["./edit-profile.page.scss"]
})
export class EditProfilePage implements OnInit {
  fakeImage: boolean = true;
  options: any;
  imageResponse: any;

  constructor(
    public photoService: PhotoServiceService,
    private storage: Storage,
    private route: Router,
    public alertController: AlertController,
    private imagePicker: ImagePicker
  ) {}

  ngOnInit() {
    this.photoService.loadSaved();
    for (var val of this.photoService.photos) {
      if (val.data) {
        this.fakeImage = false;
      }
    }
  }

  clear() {
    this.fakeImage = false;
  }
  updatebut() {
    //this.photoService.loadSaved();
    this.route.navigate(["/dash-board"]);
  }

 
  getImages() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      //maximumImagesCount: 3,
 
      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
    
      quality: 25,
 
     
      outputType: 1
    };
    this.imageResponse = [];
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }
 

  async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
     
      buttons: [
        {
          text: "Take Photo",
          handler: () => {
            this.photoService.openCam();
          }
        },
        {
          text : "Choose From Gallery",
          handler: () => {
           this.chooseFromgallery();
          }
        }
      ]
    });

    await alert.present();
  }

}
