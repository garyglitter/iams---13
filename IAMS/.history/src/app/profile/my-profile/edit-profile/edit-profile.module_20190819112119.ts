import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditProfilePage } from './edit-profile.page';
import { PhotoServiceService } from 'src/app/commonService/photo-service.service';
import { MyProfilePageModule } from '../my-profile.module';

const routes: Routes = [
  {
    path: '',
    component: EditProfilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyProfilePageModule,
    RouterModule.forChild(routes)
  ],
providers:[PhotoServiceService],
  declarations: [EditProfilePage]
})
export class EditProfilePageModule {}
