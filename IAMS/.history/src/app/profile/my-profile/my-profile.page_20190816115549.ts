import { Component, OnInit } from "@angular/core";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { PhotoServiceService } from "src/app/commonService/photo-service.service";
@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.page.html",
  styleUrls: ["./my-profile.page.scss"]
})
export class MyProfilePage implements OnInit {
  fakeImage: boolean;
  constructor(public photoService: PhotoServiceService) {
    if (this.photoService.photos.length > 0) {
      this.fakeImage = false;
    }
  }
  ngOnInit() {
    this.photoService.loadSaved();
   
  }
  onClick()
  {
    this.fakeImage = false;
  }
}
