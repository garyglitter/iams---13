import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-pending-approval',
  templateUrl: './my-pending-approval.page.html',
  styleUrls: ['./my-pending-approval.page.scss'],
})
export class MyPendingApprovalPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  // tslint:disable-next-line: member-ordering
  count = [1,2,3,4,5,6,7,8,9,10,11,12];
}
