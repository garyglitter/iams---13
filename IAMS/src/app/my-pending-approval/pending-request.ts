export class PendingRequest {
    id: number | string;
    accessType :string;
    transactionNumber :string;
    dateAndTime :string;
    description :string;
}
