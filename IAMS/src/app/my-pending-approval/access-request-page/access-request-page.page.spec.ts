import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessRequestPagePage } from './access-request-page.page';

describe('AccessRequestPagePage', () => {
  let component: AccessRequestPagePage;
  let fixture: ComponentFixture<AccessRequestPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessRequestPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessRequestPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
