import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccessRequestPagePage } from './access-request-page.page';

const routes: Routes = [
  {
    path: '',
    component: AccessRequestPagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AccessRequestPagePage]
})
export class AccessRequestPagePageModule {}
